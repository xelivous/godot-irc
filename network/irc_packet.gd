extends Resource
# defines a compliant IRC packet

# a hilarious regex from https://gist.github.com/datagrok/380449c30fd0c5cf2f30
# /(?::(([^@!\ ]*)(?:(?:!([^@]*))?@([^\ ]*))?)\ )?([^\ ]+)((?:\ [^:\ ][^\ ]*){0,14})(?:\ :?(.*))?$/

# have to escape escapes and quotations for gdscript
# also doesn't require to be wrapped in /
const REGEX = "(?::(([^@!\\ ]*)(?:(?:!([^@]*))?@([^\\ ]*))?)\\ )?([^\\ ]+)((?:\\ [^:\\ ][^\\ ]*){0,14})(?:\\ :?(.*))?$"
var _regex #created on first call to parse

const MAX_LENGTH = 512 #including trailing \r\n
const MAX_PARAMS = 14
const PREFIX_CHAR = ":"
const CRLF = "\r\n"

var _valid = false setget ,is_valid

var _raw = "" setget , get_raw_string
var _prefix = "" setget set_prefix, get_prefix #the complete prefix string
var _nick = "" setget set_nick, get_nick #or the servername
var _username = "" setget set_username, get_username
var _hostname = "" setget set_hostname, get_hostname
var _command = "" setget set_command, get_command
var _params = [] setget set_params, get_params
var _trailing = "" setget set_trailing, get_trailing
var _trailing_colon = false setget set_trailing_colon

# parses a string and turns it into the variables above
func parse(string):
	if(typeof(_regex) == TYPE_NIL):
		_regex = RegEx.new()
		_regex.compile(REGEX)

	var result = _regex.search(string)

	# set whether or not we're valid
	_valid = (typeof(result) != TYPE_NIL)

	if(is_valid()):
		var array = result.get_group_array()

		_raw = string
		_prefix = array[0]
		_nick = array[1]
		_username = array[2]
		_hostname = array[3]
		_command = array[4]
		_params = array[5].split(" ", false) #split by space and ignore "empty" params
		_trailing = array[6]

		var trail_last = string.find_last(":")
		if(trail_last > 0): #not prefix colon
			_trailing_colon = true

# turns the variables into a string for sending
# also returns the compiled output as a convenience
# only add a colon before trailing if desired
func compile():
	var output = "%s%s%s%s\r\n"
	var prefix = get_prefix()
	var command = get_command()
	var params = get_params()
	var trailing = get_trailing()

	if(prefix.length() > 0):
		prefix = ":" + prefix + " "

	# convert the params array into a spaced string
	var temp = ""
	for x in params:
		temp = temp + " " + x
	params = temp

	if(trailing.length() > 0):
		if(_trailing_colon):
			trailing = ":" + trailing
		trailing = " " + trailing

	_raw = output % [prefix,command,params,trailing]

	# just shrink the output on max length for now...?
	if(_raw.length() > MAX_LENGTH):
		printt("Packet(%s) tried to compile but was over the max length" % self)
		_raw = _raw.left(MAX_LENGTH - 2) + "\r\n"
		printt("New length is %d" % _raw.length())

	return _raw

# there's a few characters that shouldn't exist normally...
func _sanitize(s):
	# NUL, CR, LF, " " and ":"
	s = s.replace(" ", "")
	s = s.replace(":", "")
	s = s.replace("\r", "")
	s = s.replace("\n", "")
	#s = s.replace("\x00", "") #should never contain null because lolstrings
	#s = s.c_escape()
	return s

###
# outward-facing functions to allow after the fact editing...
# for easy generation of compliant packet sending...?
func is_valid():
	return _valid

func get_raw_string():
	return _raw

func get_prefix():
	return _prefix
# tbh clients shouldn't really be setting a prefix
func set_prefix(prefix):
	printt("setting prefix", prefix)
	# is a string and doesn't contain a space
	if(typeof(prefix) == TYPE_STRING and prefix.find(" ") < 0):
		# remove preceding colon since we add it ourselves when compiling
		if(prefix.begins_with(":")):
			prefix = prefix.right(1)

		_prefix = ""

		var split = prefix.split("@")
		var had_host = false
		# if we have a hostname
		if(split.size() > 1):
			had_host = true
			set_hostname(split[1])
			_prefix = "@" + split[1]
		else:
			set_hostname("")

		split = split[0].split("!")
		# if we have a username and we previously found a hostname
		if(had_host and split.size() > 1):
			set_username(split[1])
			_prefix = "!" + split[1] + _prefix
		else:
			set_username("")

		set_nick(split[0])

		_prefix = _sanitize(split[0] + _prefix)

func get_nick():
	return _nick
func set_nick(s):
	_nick = _sanitize(s.strip_edges())

func get_username():
	return _username
func set_username(s):
	_username = _sanitize(s.strip_edges())

func get_hostname():
	return _hostname
func set_hostname(s):
	_hostname = _sanitize(s.strip_edges())

func get_command():
	return _command
func set_command(s):
	_command = _sanitize(s.strip_edges())

func get_params():
	return _params
func set_params(array):
	# requires it to be an array
	if(typeof(array) == TYPE_ARRAY):
		var temp = []

		# it's possible that someone tries to add a space, so split it.. i guess
		for param in array:
			for split in param.split(" "):
				# should probably sanitize it sometime as well
				temp.append(_sanitize(split.strip_edges()))

		# make sure we only have MAX_PARAMS amount of params..
		if(temp.size() > MAX_PARAMS):
			temp.resize(MAX_PARAMS)

		_params = temp

func get_trailing():
	return _trailing
func set_trailing(s):
	# we don't sanitize the end??
	if(s.begins_with(":")):
		set_trailing_colon(true)
		s = s.right(1)

	_trailing = s.strip_edges()

func set_trailing_colon(b):
	_trailing_colon = b