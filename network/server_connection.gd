extends Node
# Defines an IRC server connection and emits signals when things happen

onready var thread = Thread.new()
onready var mutex = Mutex.new()
onready var connection = StreamPeerTCP.new()

var _outgoing = []
var _incoming = []
var _shutdown = false

# defines a packet to/from IRC server
var packet_obj = preload("irc_packet.gd")

signal terminate_me() #called when a thread wants to be joined to main thread again
signal server_connected() #called directly after the server was connected to in order to initiate basic stuff
signal packet_receive(packet) # called whenever a packet is received

func _ready():
	set_process(true)
	_connect("127.0.0.1")

	# automatic shutdown after time for a test
	#get_tree().create_timer(1).connect("timeout", self, "shutdown")

# handle our network functions on the main thread
# might need to batch requests somehow for fast moving connections...?
func _process(delta):
	var packet
	mutex.lock()
	if(_incoming.size() > 0):
		packet = _incoming.pop_front()
	mutex.unlock()

	if(typeof(packet) != TYPE_NIL):
		emit_signal("packet_receive", packet)

func _connect(ip, port=6667):
	thread.start(self, "_thread", {"ip": ip, "port": port})

# handles the actual rejoining of a thread
func _terminate():
	if(thread.is_active()):
		var id = thread.get_id()
		printt("Terminating [%s] as requested" % id)
		thread.wait_to_finish()
		printt("Successful termination [%s]" % id)

func shutdown():
	mutex.lock()
	_shutdown = true
	mutex.unlock()

func new_packet(string=""):
	var obj = packet_obj.new()
	if(string != ""):
		obj.parse(string)
	return obj

func send_packet(packet):
	mutex.lock()
	_outgoing.append(packet)
	mutex.lock()
	pass

###
# everything below this point is our networked thread
func _thread(data):
	var thread_id = "[%s:%d]" % [data.ip, data.port]

	var status = connection.connect(data.ip, data.port)
	printt("%s Successful connection = %s" % [thread_id, str(status == OK)])
	if(status == OK):
		emit_signal("server_connected")

	_shutdown = status
	while(!_shutdown):
		# check for incoming packets
		_handle_incoming()

		# send outgoing packets
		_handle_outgoing()
		OS.delay_msec(100)

	# shutdown was called somewhere so let's shut us down
	printt("%s shutting down?" % thread_id)
	call_deferred("_terminate")
	return true

func _handle_incoming():
	var bytes = connection.get_available_bytes()
	if (bytes > 0):
		var string = connection.get_utf8_string(bytes)
		var strings = string.split("\r\n", false) #ignore empty
		for s in strings:
			# create a new package object and tell it to parse our string
			var packet = packet_obj.new()
			packet.parse(s)
			_incoming.append(packet)

func _handle_outgoing():
	if(_outgoing.size() > 0):
		var output = _outgoing.pop_front()
		#output.compile()
		printt("[Sending] %s" % output.compile().replace("\r\n", "\\r\\n"), " ")

		#utf8_string appends a null byte which is *wrong*
		#connection.put_utf8_string(output.compile())
		var bytes = var2bytes(output.compile())
		var err = connection.put_data(bytes)
